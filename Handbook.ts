//parameter type annotation in object type

function coordinates(pt: { x: number; y: number }) {
    console.log(pt.x + ' ' + pt.y)
}

coordinates({ x: 12, y: 15 });

//function type expression

type start = (first: string) => void;

function greeting(fn: start) {
    fn("function");
}

function printconsole(str: string) {
    console.log(str)
}

greeting(printconsole);

//rest parameter

function rest(x: number, ...y: number[]) {
    console.log(x + ' ' + y)
    return y.map(s => s * x)
}

const mul = rest(1, 2, 3, 4)
console.log(mul)

//function overloads

function len(s: string): number;
function len(arr: any[]): number;
function len(x: any) {
    return x.length;
}

console.log(len("sundar"));
console.log(len([0]));

//optional properties

interface PaintOptions {
    xPos?: number;
    yPos?: number;
}

function paintShape(opts: PaintOptions) {
    return opts;
}

console.log(paintShape({ xPos: 100 }));
console.log(paintShape({ yPos: 100 }));
console.log(paintShape({ xPos: 100, yPos: 100 }));