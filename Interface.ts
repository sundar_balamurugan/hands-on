interface user {
    name: string,
    id: number
}

//variable should be declared that should match the interface definition......

const user: user = {
    name: "sundar",
    id: 1
}

console.log(user.id + ' ' + user.name);

// class

interface data {
    name: string,
    id: number
}

class UserData {
    name: string
    id: number
    constructor(name: string, id: number) {
        this.name = name;
        this.id = id;
    }
}

const User: data = new UserData("bala", 2)

console.log(User.id + ' ' + User.name);