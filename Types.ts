// Unions
const arr = "sundar"

console.log(getlength(arr))

function getlength(obj: number | string) {
    return obj;
}

//Structural type system

interface Point {
    x: number;
    y: number;
}

function logPoint(p: Point) {
    console.log(`${p.x}, ${p.y}`);
}

const point = { x: 12, y: 26 };
logPoint(point);
