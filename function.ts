//structural Typing

type A = { val: string }

interface b {
    val: string
}

class C {
    val = "Welcome to Typesctipt";
}

let x: A = { val: "Hello" }
let y: b = x;
y = new C();
console.log(y)

//Discriminated unions

type Shape =
    | { kind: "circle"; radius: number }
    | { kind: "square"; x: number }
    | { kind: "triangle"; x: number; y: number };

function area(s: Shape) {
    if (s.kind === "circle") {
        return Math.PI * s.radius * s.radius;
    } else if (s.kind === "square") {
        return s.x * s.x;
    } else {
        return (s.x * s.y) / 2;
    }
}

