interface Person {
    firstname: string
    lastname: string
}

class Data {
    fullname: string;
    constructor(
        public firstname: string,
        public middlename: string,
        public lastname: string
    ) {
        this.fullname = firstname + "" + middlename + "" + lastname;
    }
}

function personaldata(person: Person) {
    return "Hi, " + person.firstname + " " + person.lastname;
}

let personal = new Data("sundar", "B.", "Bala")

console.log(personaldata(personal))