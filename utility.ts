//partial type

interface book {
    title: string,
    author: string
}

function getbook(details: book, par: Partial<book>) {
    return { ...details, ...par }
}

const display = {
    title: "marvel",
    author: "micheal"
}

console.log(getbook(display, {
    title: "DC"
}))

//Record type

type authordetail = 'sundar' | 'siva' | 'rakesh'

const bookdata: Record<authordetail, book> = {
    sundar: { title: 'Hindu', author: 'baahu' },
    siva: { title: 'bahubali', author: 'kattapa' },
    rakesh: { title: 'ghost', author: 'veera' }
};

console.log(bookdata)

//pickup type

type titleOnly = Pick<book, "title">

const titleData: titleOnly = {
    title: "walker"
}
console.log(titleData);