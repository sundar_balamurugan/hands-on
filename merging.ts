//merging interfaces

interface box {
    height: number,
    width: number
}

interface box {
    scale: number
}

const measurement: box = { height: 4, width: 6, scale: 12 };

//merging namespace

namespace Animal {
    export let haveMuscles = true;
    export function animalsHaveMuscles() {
        return haveMuscles;
    }
}
namespace Animal {
    export function doAnimalsHaveMuscles() {
        return haveMuscles;
    }
}

//merging namesapce with enum

enum Color {
    red = 1,
    green = 2,
    blue = 4,
}
namespace Color {
    export function mixColor(colorName: string) {
        if (colorName == "yellow") {
            return Color.red + Color.green;
        } else if (colorName == "white") {
            return Color.red + Color.green + Color.blue;
        } else if (colorName == "magenta") {
            return Color.red + Color.blue;
        } else if (colorName == "cyan") {
            return Color.green + Color.blue;
        }
    }
}